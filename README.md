# Welcome

#### Hi, my name is Maxim and I am from Russia.

## My projects

[![OKAI Bot](https://img.shields.io/badge/-OKAI%20Bot-05122A?logo=Discord&style=for-the-badge)](https://top.gg/bot/666151106958852116)
[![Shiro Chan](https://img.shields.io/badge/-Shiro%20Chan-05122A?logo=Discord&style=for-the-badge)](https://github.com/DMax-YT/shiro-chan)
[![Let me search that for you](https://img.shields.io/badge/-Let%20me%20search%20that%20for%20you-05122A?logo=Google&style=for-the-badge)](https://dmax-yt.github.io/let-me-search-that-for-you)

## Some technologies

![HTML](https://img.shields.io/badge/-HTML-05122A?logo=HTML5&style=for-the-badge)
![CSS](https://img.shields.io/badge/-CSS-05122A?logo=CSS3&style=for-the-badge)
![JavaScript](https://img.shields.io/badge/-JavaScript-05122A?logo=JavaScript&style=for-the-badge)
![Node.js](https://img.shields.io/badge/-Node.js-05122A?logo=Node.js&style=for-the-badge)
![Vue 3](https://img.shields.io/badge/-Vue%203-05122A?logo=Vue.js&style=for-the-badge)
![Postgresql](https://img.shields.io/badge/-Postgresql-05122A?logo=Postgresql&style=for-the-badge)

## Support me

[![Patreon](https://img.shields.io/badge/-Patreon-05122A?logo=Patreon&style=for-the-badge)](https://www.patreon.com/dmax_programmer)
[![Boosty](https://img.shields.io/badge/-Boosty-05122A?style=for-the-badge)](https://boosty.to/DMax)

## Contact me

![My discord is DMax#3317](https://img.shields.io/badge/-DMax%233317-05122A?logo=Discord&style=social)
![My twitter is DMax_Programmer](https://img.shields.io/badge/-DMax__Programmer-05122A?logo=Twitter&style=social)
